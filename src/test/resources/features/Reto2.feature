#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario: Title of your scenario
    Given I want to write a step with precondition
    And some other precondition
    When I complete action
    And some other action
    And yet another action
    Then I validate the outcomes
    And check more outcomes

  @Credito
  Scenario Outline: Tarjetas de credito
    Given ingresar a la pagina web
    And Ingresar a la opción productos y servicios
    And ingresar a la opcion tarjetas de credito
    And darle click al boton solicitala aqui
    When Se ingresa campos del formulario 
    | <Nombre> 	 | <Apellido> | <TipoDoc>  |	<NumDoc> | <FechaNacimiento>| <IngresosMensuales>	| <CiudadDepto>	|
    Then Dar click en continuar

    Examples: 
      | Nombre 	 | Apellido	 | TipoDoc  						|NumDoc			|FechaNacimiento	|IngresosMensuales	|CiudadDepto					|
      | Daniel	 | Yepes		 | Cedula de Ciudadanía |1039472881	|1990-01-06				|1000000						|Envigado - Antioquia	|
