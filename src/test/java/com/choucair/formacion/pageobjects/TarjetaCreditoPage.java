package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class TarjetaCreditoPage extends PageObject {

	
	//Productos y servicios
	@FindBy(xpath="//*[@id='main-menu']/div[2]/ul[1]/li[3]/a")
	public WebElement productosyServicios;
	
	@FindBy(xpath="//*[@id='productosPersonas']/div/div[1]/div/div/div[7]/div/a")
	public WebElement tarjetasCredito;
	
	@FindBy(xpath="//*[@id='card_0']/div[2]/h2")
	public WebElement name1;
	
	@FindBy(xpath="//*[@id='card_1']/div[2]/h2")
	public WebElement name2;
	
	@FindBy(xpath="//*[@id='card_0']/div[3]/ul/li[1]")
	public WebElement infoAmerican1;
	
	@FindBy(xpath="//*[@id='card_0']/div[3]/ul/li[2]")
	public WebElement infoAmerican2;
	
	@FindBy(xpath="//*[@id='card_0']/div[3]/ul/li[3]")
	public WebElement infoAmerican3;
	
	
	@FindBy(xpath="//*[@id='card_1']/div[3]/ul/li[1]")
	public WebElement masterBlack1;
	
	@FindBy(xpath="//*[@id='card_1']/div[3]/ul/li[2]")
	public WebElement masterBlack2;
	
	@FindBy(xpath="//*[@id='card_1']/div[3]/ul/li[3]")
	public WebElement masterBlack3;
	
	@FindBy(xpath="//*[@id='card_0']/div[4]/a")
	public WebElement botonSolicitalaAqui;
	
	@FindBy(xpath="//*[@id='conIframe_rm']/div/form/div[7]/div[2]/button")
	public WebElement botonContinuar;
	
	@FindBy(name="nombresReq")
	public WebElement nombres;
	
	@FindBy(name="apellidosReq")
	public WebElement apellidos;
	
	@FindBy(name="typedocreq")
	public WebElement tipoDoc;

	@FindBy(name="numeroDocumento")
	public WebElement numeroDocumento;

	@FindBy(id="fechaNacimientoReq")
	public WebElement fechaNacimiento;
	
	@FindBy(id="ingresos-mensuales")
	public WebElement ingresos;
	
	@FindBy(id="reqCiuidadDpto_value")
	public WebElementFacade ciudepto;
	
	@FindBy(xpath="//*[@id='reqCiuidadDpto_dropdown']/div[3]/div")
	public WebElementFacade selciu;
	
	
	public void ingresaProductosyServicios() {
		productosyServicios.click();
	}

	public void ingresaTarjetaDeCredito()  {
		tarjetasCredito.click();
		System.out.println(name1.getText());
		System.out.println("AmericaExpress"+infoAmerican1.getText());
		System.out.println("AmericaExpress"+infoAmerican2.getText());
		System.out.println("AmericaExpress"+infoAmerican3.getText());
		System.out.println(name2.getText());
		System.out.println("MasterBlack"+masterBlack1.getText());
		System.out.println("MasterBlack"+masterBlack2.getText());
		System.out.println("MasterBlack"+masterBlack3.getText());
	}

	public void darClickSolicitalaAqui() {
		botonSolicitalaAqui.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getDriver().switchTo().frame(getDriver().findElement(By.tagName("iframe")));
	}
	
	public void clickContinuar() {
	botonContinuar.click();
	}
	

	public void setNombre(String datoPrueba) {
		nombres.sendKeys(datoPrueba);
	}
	
	public void setApellido(String datoPrueba) {
		apellidos.sendKeys(datoPrueba);
	}
	
	public void settipoDoc(String datoPrueba) {
		tipoDoc.sendKeys(datoPrueba);
	}
	public void setnumeroDocumento(String datoPrueba) {
		numeroDocumento.sendKeys(datoPrueba);
	}
	public void setfechaNacimiento(String datoPrueba) {
		fechaNacimiento.sendKeys(datoPrueba);
	}
	
	public void setIngresos(String datoPrueba) {
		ingresos.sendKeys(datoPrueba);
	}

	public void setCiuDepto(String  datoPrueba) {
		ciudepto.sendKeys(datoPrueba);
		selciu.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	


}
