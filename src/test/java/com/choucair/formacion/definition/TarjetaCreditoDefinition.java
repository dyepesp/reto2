package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.TarjetaCreditoSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class TarjetaCreditoDefinition {

	@Steps
	TarjetaCreditoSteps tarjetaCreditoSteps;
	
	@Given("^ingresar a la pagina web$")
	public void ingresar_a_la_pagina_web() throws Throwable {
		tarjetaCreditoSteps.ingresarALaWeb();
	}
	
	@Given("^Ingresar a la opción productos y servicios$")
	public void ingresar_a_la_opción_productos_y_servicios() throws Throwable {
		tarjetaCreditoSteps.ingresarProductosyServicios();
	}
	
	@Given("^ingresar a la opcion tarjetas de credito$")
	public void ingresar_a_la_opcion_tarjetas_de_credito() throws Throwable {
		tarjetaCreditoSteps.ingresaraTarjestasDeCredito();
	}
	
	@Given("^darle click al boton solicitala aqui$")
	public void darle_click_al_boton_solicitala_aqui() throws Throwable {
	   tarjetaCreditoSteps.darClickEnSolicitaAqui();
	}
	
	@When("^Se ingresa campos del formulario$")
	public void se_ingresa_campos_del_formulario(DataTable dbdata) throws Throwable {
		List<List<String>> data = dbdata.raw();
		for( int i=0; i<data.size(); i++) {
			tarjetaCreditoSteps.diligenciar_formulario(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {}
		}
	}
	
	@Then("^Dar click en continuar$")
	public void dar_click_en_continuar() throws Throwable {
		tarjetaCreditoSteps.darClickenContinuar();
	}
}
