package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.TarjetaCreditoPage;

import net.thucydides.core.annotations.Step;

public class TarjetaCreditoSteps {
	
	TarjetaCreditoPage tarjetaCreditoPage;

	@Step
	public void ingresarALaWeb() {
		tarjetaCreditoPage.open();
	}

		
	@Step
	public void ingresarProductosyServicios() {
		tarjetaCreditoPage.ingresaProductosyServicios();
	}


	public void ingresaraTarjestasDeCredito() {
		tarjetaCreditoPage.ingresaTarjetaDeCredito();
		
	}


	public void darClickEnSolicitaAqui() {
		tarjetaCreditoPage.darClickSolicitalaAqui();
	}


	public void diligenciar_formulario(List<List<String>> data, int i) {
		tarjetaCreditoPage.setNombre(data.get(i).get(0).trim());
		tarjetaCreditoPage.setApellido(data.get(i).get(1).trim());
		tarjetaCreditoPage.settipoDoc(data.get(i).get(2).trim());
		tarjetaCreditoPage.setnumeroDocumento(data.get(i).get(3).trim());
		tarjetaCreditoPage.setfechaNacimiento(data.get(i).get(4).trim());
		tarjetaCreditoPage.setIngresos(data.get(i).get(5).trim());
		tarjetaCreditoPage.setCiuDepto(data.get(i).get(6).trim());
	}


	public void darClickenContinuar() {
		tarjetaCreditoPage.clickContinuar();
	}

}
